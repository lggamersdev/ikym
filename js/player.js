var PlayerIKYM = function(game) {
	
}


gameState.prototype = {
	nextTile: undefined,
	sprite: undefined,
	castPlayer: undefined,
	createItem: function() {
		// choosing an empty tile in the field
		do{
			var col = Math.floor(Math.random()*8);
			var lin = Math.floor(Math.random()*4);
		} while (this.getPlayerAt(col,lin) || this.getItemAt(col, item))

		var item = this.add.sprite(
				col*TILE_SIZE + TILE_SIZE/2,
				lin*TILE_SIZE + TILE_SIZE/2,
				"bomb_item"
			);
		item.name = "bomb_item";
		// creation of a custom property "pos"
		item.tablePosition = {
			col: col,
			lin: lin
		};
		item.anchor.set(0.5);
		
		// at the beginning the tile is completely transparent
		item.alpha=0;
		// creation of a new tween for the tile sprite
		var fadeIn = this.add.tween(item);
		// the tween will make the sprite completely opaque in 250 milliseconds
		fadeIn.to({alpha:1},250);
		// starting the tween
		fadeIn.start();

		itemsGroup.add(item);
		return item;
	},
	// FUNCTION TO MOVE A PLAYER
	movePlayer: function(player, callback){
		var to = player.nextTile.tablePosition;
		// then we create a tween
		var movement = this.add.tween(player.sprite);
		var distance2= 1.0*(
				  Math.pow(to.lin - player.tablePosition.lin, 2)
				+ Math.pow(to.col - player.tablePosition.col, 2)
			);
		movement.to(
			{
				x:TILE_SIZE*to.col + TILE_SIZE/2,
				y:TILE_SIZE*to.lin + TILE_SIZE/2
			}
			, 200*Math.sqrt(distance2,2)
		);
		movement.onComplete.add(function(){
			this.unselectTile(table[player.playerData.tablePosition.lin][player.playerData.tablePosition.col]);
			if(callback) {
				callback.call(this);
			}
		}, this);
		player.playerData.tablePosition=to;
		player.nextTile = false;
		// let the tween begin!
		movement.start();
	},
	consumeBomb: function(player, callback) {
		var counter = 0;
		var callbackHub = function() {
			counter -= 1;
			console.log(counter);
			if (counter == 0) {
				callback.call(this);
			};
		};
		var tablePosition = player.playerData.tablePosition;
		if(tablePosition.col > 0) {
			if(tablePosition.lin > 0) {
				counter += 1;
				this.explode(tablePosition.col -1, tablePosition.lin -1, callbackHub);
			}
			if(tablePosition.lin < LINES - 1) {
				counter += 1;
				this.explode(tablePosition.col -1, tablePosition.lin +1, callbackHub);
			}
		}
		if(tablePosition.col < COLUMNS - 1) {
			if(tablePosition.lin > 0) {
				counter += 1;
				this.explode(tablePosition.col +1, tablePosition.lin -1, callbackHub);
			}
			if(tablePosition.lin < LINES - 1) {
				counter += 1;
				this.explode(tablePosition.col +1, tablePosition.lin +1, callbackHub);
			}
		}
	},
	explode: function(col, lin, callback) {
		var player = this.getPlayerAt(col,lin);
		if(player) {
			this.hitPlayer(player, 10);
		}

		// explosion animation
		var explosionAnimation = explosions.getFirstExists(false);
		explosionAnimation.reset(
				col*TILE_SIZE + TILE_SIZE/2,
				lin*TILE_SIZE + TILE_SIZE/2);
		explosionAnimation.animations.getAnimation('kaboom')
				.onComplete.addOnce(callback, this);
		explosionAnimation.play('kaboom', 30, false, true);
	},
	hitPlayer: function(player, damage) {
		player.sprite.damage(damage);
		var textSprite = player.sprite.children[0];
		textSprite.setText(player.sprite.health);
	},
	selectTile: function(tileSprite, text) {
		if(tileSprite.isSelected) {
			var textSprite = tileSprite.children[0];
			textSprite.setText(text);
		} else {
			// creation of a text which will represent the value of the tileSprite
			var textSprite = this.add.text(
					TILE_SIZE/2,
					TILE_SIZE/2,
					text,
					{ font:"bold 16px Arial", align:"center" }
				);
			// setting text anchor in the horizontal and vertical center
			textSprite.anchor.set(0.5);
			// adding the text as a child of tile sprite
			tileSprite.addChild(textSprite);

			// creation of a new tween for the tile sprite
			// IDLE -> SELECTED
			var clickOn = this.add.tween(tileSprite);
			clickOn.to({alpha:0.5},50);
			//clickOn.onComplete.add(function(){}, this);
			clickOn.start();
			tileSprite.isSelected = true;
		}
	},
	unselectTile: function(tileSprite) {
		if(tileSprite.isSelected) {
			// creation of a new tween for the tile sprite
			// SELECTED -> IDLE
			var clickOff = this.add.tween(tileSprite);
			clickOff.to({alpha:1.0},150);
			clickOff.start();

			tileSprite.children[0].destroy();
			tileSprite.isSelected = false;
		}
	}
};
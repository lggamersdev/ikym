// Copyright 2015 Google Inc. All Rights Reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
'use strict';

var game = null;
var gameManager = null;

/**
 * Main entry point. This is not meant to be compiled so suppressing missing
 * goog.require checks.
 */
var initialize = function() {
  var castReceiverManager = cast.receiver.CastReceiverManager.getInstance();
  var appConfig = new cast.receiver.CastReceiverManager.Config();

  appConfig.statusText = 'IKYM';
  // In production, use the default maxInactivity instead of using this.
  appConfig.maxInactivity = 6000;

  // Create the game before starting castReceiverManager to make sure any extra
  // cast namespaces can be set up.
  /** @suppress {missingRequire} */
  var gameConfig = new cast.receiver.games.GameManagerConfig();
  gameConfig.applicationName = 'IKYM';
  gameConfig.maxPlayers = 4;

  /** @suppress {missingRequire} */
  gameManager = new cast.receiver.games.GameManager(gameConfig);

  var GLOBALS = {
    TILE_SIZE: 100,
    COLUMNS: 8,
    LINES: 4
  };
  
  // creation of a new phaser game, with a proper width and height according to tile size
  /** @suppress {missingRequire} */
  var phaserGame = new Phaser.Game(
    GLOBALS.TILE_SIZE*GLOBALS.COLUMNS,
    GLOBALS.TILE_SIZE*GLOBALS.LINES,
    Phaser.CANVAS, "");

  phaserGame.state.add("boot",bootState);
  phaserGame.state.add("preload",preloadState);
  phaserGame.state.add("join",joinState);
  phaserGame.state.add("game",gameState);
  

  var startGame = function() {
    phaserGame.state.start("boot", true, false, gameManager);
    gameManager.updateGameStatusText('Game running.');
  };

  castReceiverManager.onReady = function(event) {
    if (document.readyState === 'complete') {
      startGame();
    } else {
      window.onload = startGame;
    }
  };
  castReceiverManager.start(appConfig);
};

if (document.readyState === 'complete') {
  initialize();
} else {
  /** Main entry point. */
  window.onload = initialize;
}

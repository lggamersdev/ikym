JoinStateListener = function(state) {
	this.state = state;
};

JoinStateListener.prototype = {
	onPlayerAvailable: function(event) {
		console.log('Player ' + event.playerInfo.playerId + ' is available');
		console.log("available.!");
		event.playerInfo.playerData = { name: "--//--" };


		this.state.updatePlayersTable.call(this.state);
	},
	onPlayerReady: function(event) {
		console.log('Player ' + event.playerInfo.playerId + ' is ready');
		console.log("ready.!");
		console.log(event);
		event.playerInfo.playerData = {};
		event.playerInfo.playerData.name = event.requestExtraMessageData.name;

		this.state.updatePlayersTable.call(this.state);
		var playersAvailable = gameManager.getPlayersInState(cast.receiver.games.PlayerState.AVAILABLE).length;
		var playersReady     = gameManager.getPlayersInState(cast.receiver.games.PlayerState.READY).length;
		if(playersReady > 1
			&& playersAvailable == 0) {
			this.state.playersReady();
		}
	},
	onPlayerIdle: function() {},
	onPlayerPlaying: function() {},
	onPlayerDropped: function() {},
	onPlayerQuit: function() {},
	onPlayerDataChanged: function() {},
	onGameStatusTextChanged: function() {},
	onGameMessageReceived: function(event) {
		console.log(event);
	},
	onGameDataChanged: function() {},
	onGameLoading: function() {},
	onGameRunning: function() {},
	onGamePaused: function() {},
	onGameShowingInfoScreen: function() {},
	onLobbyOpen: function() {},
	onLobbyClosed: function() {}
};


var joinState = function(){
	console.log("joinState");
	this.playersText = undefined;
	this.joinStateListener = new JoinStateListener(this);
};
  
joinState.prototype = {
	preload: function(){},
	create: function(){
		//this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		//this.scale.pageAlignHorizontally = true;
		splash = this.add.image(0, 0, "gametitle");

		var style = { font: "16px Courier", fill: "#fff", tabs: [ 164, 120, 80 ] };

		var headings = [ 'STATUS', 'ID', 'NAME' ];

		var text = this.add.text(32, 100, '', style);
		text.parseList(headings);

		this.playersText = this.add.text(32, 130, '', style);

		gameManager.addGameManagerListener(this.joinStateListener);

		console.log(gameManager.getPlayers());
		var players = gameManager.getPlayers();
		for (var i = 0; i < players.length; i++) {
			var player = players[i];
			player.playerData = { name: "--//=="};
		};
		this.updatePlayersTable();
	},
	updatePlayersTable: function() {
		var playersArray = [];
		var gmPlayers = gameManager.getPlayers();
		for (var i = 0; i < gmPlayers.length; i++) {
			var player = gmPlayers[i];
			playersArray.push([
					player.playerState,
					player.playerId,
					player.playerData.name
				]);
		};
		this.playersText.parseList(playersArray);
	},
	playersReady: function() {
		this.state.start("game");
		// this.state.start("game", true, false, players);
	}
}


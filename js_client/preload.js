var preloadState = function(game){
	console.log("preloadState");
}

preloadState.prototype = {
	preload: function(){ 
		var loadingBar = this.add.sprite(160,240,"loading");
		loadingBar.anchor.setTo(0.5,0.5);
		this.load.setPreloadSprite(loadingBar);
		this.game.load.spritesheet("numbers","assets/numbers.png",100,100);
		this.game.load.image("gametitle","assets/gametitle.png");
		this.game.load.image("play","assets/play.png");
		this.game.load.image("higher","assets/higher.png");
		this.game.load.image("lower","assets/lower.png");
		this.game.load.image("gameover","assets/gameover.png");

		
		// preload the only image we are using in the game
		this.load.image("tile", "imgs/tile.png");
		this.load.image("player", "imgs/tank1.png");
		this.load.image("bomb_item", "imgs/bomb.png");
		this.load.spritesheet('kaboom', 'imgs/explosion.png', 64, 64, 23);
	},
  	create: function(){
		this.game.state.start("join");
	}
}
var joinState = function(game){
	console.log("joinState");
	players = [];
	playersText = undefined;
};
  
joinState.prototype = {
	preload: function(){},
	create: function(){
		console.log('### Sending AVAILABLE message.');
		gameManagerClient.sendPlayerAvailableRequest(null);

		var style = { font: "16px Courier", fill: "#fff", tabs: [ 164, 120, 80 ] };

		this.add.text(32, 70, 'Name:', style);

		this.name = "";
		this.text = this.add.text(32, 100, '', style);

		// playerReadyGame = this.input.keyboard.addKey(Phaser.Keyboard.R);
		// playerReadyGame.onDown.add(this.sendReadyMessage,this);

		this.input.keyboard.addCallbacks(this, null, null, this.keyPress);
	},
	keyPress: function(char) {
		a = char;
		console.log(char);
		if (char.charCodeAt(0) == 13) {
			this.text.tint = parseInt("0xaadd00");
			this.sendReadyMessage();
			return;
		};
		this.name += char;
		this.text.setText(this.name);
	},
	onPlayersReady: function() {
		this.state.start("game", true, false, players);
	},
	sendReadyMessage: function() {
		var message = {
			name: this.name
		};
		gameManagerClient.sendPlayerReadyRequest(message);
	}
}


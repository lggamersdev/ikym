var phaserGame;
/** @define {string} Application ID used when running the sender. */
var APP_ID = '457E24FE';
// Global reference to game session manager for console debugging.
var gameManagerClient = null;

var initGame = function () {
	if(phaserGame) {
		console.log("Game already started.");
		return;
	}
	
	var GLOBALS = {
		TILE_SIZE: 100,
		COLUMNS: 8,
		LINES: 4
	};
	
	// creation of a new phaser game, with a proper width and height according to tile size
	phaserGame = new Phaser.Game(
		GLOBALS.TILE_SIZE*GLOBALS.COLUMNS,
		GLOBALS.TILE_SIZE*GLOBALS.LINES,
		Phaser.CANVAS, "");

	phaserGame.state.add("cast_connection",castConnectionState);
	phaserGame.state.add("boot",bootState);
	phaserGame.state.add("preload",preloadState);
	phaserGame.state.add("join",joinState);
	phaserGame.state.add("game",gameState);
	phaserGame.state.start("cast_connection");
	
};




/**
 * Request a cast session when Cast Sender API loads.
 * @param {boolean} loaded
 * @param {Object} errorInfo
 */
window['__onGCastApiAvailable'] = function(loaded, errorInfo) {
  if (!loaded) {
    console.error('### Cast Sender SDK failed to load:');
    console.dir(errorInfo);
    return;
  }

  cast.games.common.sender.setup(APP_ID, onSessionReady_);
};


/**
 * Callback when a cast session is ready. Connects the game manager.
 * @param {!chrome.cast.Session} session
 * @private
 */
var onSessionReady_ = function(session) {
  console.log('### Creating game manager client.');
  chrome.cast.games.GameManagerClient.getInstanceFor(session,
      function(result) {
        console.log('### Game manager client initialized!');
        gameManagerClient = result.gameManagerClient;
        phaserGame.state.start("boot");
      },
      function(error) {
        console.error('### Error initializing the game manager client: ' +
            error.errorDescription + ' ' +
            'Error code: ' + error.errorCode);
      });
};


var cast = {games:{}};
cast.games.common = {};
cast.games.common.sender = {};
/**
 * A helper that sets up the cast sender SDK with console debugging information.
 * @param {string} appId The app ID to create a cast session with.
 * @param {function(!chrome.cast.Session)} sessionCallback Callback that is
 *     called when a cast sender session is ready.
 */
cast.games.common.sender.setup = function(appId, sessionCallback) {
  console.log('### Preparing session request and cast sender API config with ' +
      'app ID ' + appId);
  var sessionRequest = new chrome.cast.SessionRequest(appId);
  var apiConfig = new chrome.cast.ApiConfig(sessionRequest, sessionCallback,
      cast.games.common.sender.setup.onCastReceiverChanged_);

  console.log('### Initializing cast sender API and requesting a session.');
  chrome.cast.initialize(apiConfig, cast.games.common.sender.setup.onCastInit_,
      cast.games.common.sender.setup.onCastError_);
};


/**
 * Callback when there is a receiver change. When the receiver is available, the
 * user can click on the chromecast button, which will create a session (and
 * call the sessionCallback passed in #setup).
 * @param {!chrome.cast.ReceiverAvailability} receiverAvailability
 * @private
 */
cast.games.common.sender.setup.onCastReceiverChanged_ = function(receiverAvailability) {
	if (receiverAvailability == chrome.cast.ReceiverAvailability.AVAILABLE) {
		console.log('\n### Click cast button in the Google Cast extension to ' +
			'start!\n');
		initGame();
	} else {
		console.log('\n### Not ready. Do NOT click cast button in the Google ' +
			'Cast extension.\n');
	}
};


/**
 * Callback when the cast API is initialized.
 * @private
 */
cast.games.common.sender.setup.onCastInit_ = function() {
  console.log('### Cast sender API initialized.');
};


/**
 * Callback when there is a cast API error.
 * @param {chrome.cast.Error} error
 * @private
 */
cast.games.common.sender.setup.onCastError_ = function(error) {
  console.log('### Cast sender API error:');
  console.dir(error);
};


var bootState = function(game){
	console.log("%cStarting my awesome game", "color:white; background:red");
	console.log("bootState");
};
  
bootState.prototype = {
	preload: function(){
		this.game.load.image("loading","assets/loading.png"); 
	},
	create: function(){
		this.game.state.start("preload");
	}
}
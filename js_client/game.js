var gameState = function(game) {
	playersGroup = undefined;
	itemsGroup = undefined;
	table = [];
	items = [];


	TILE_SIZE = 100;
	COLUMNS = 8;
	LINES = 4;

	// at the beginning of the game, the player cannot move
	canMove=false;
}


gameState.prototype = {
	init: function(args) {},
	preload: function() {},
	create: function() {
		this.createTable();
		
		// sprite group declaration
		itemsGroup = this.add.group();
		playersGroup = this.add.group();
		
		// var players = gameManager.getPlayers();
		// for (var i = 0; i < players.length; i++) {
		// 	this.createPlayer(players[i]);
		// };
	},
	createTable: function() {
		table = []
		for(lin=0; lin < LINES; lin++) {
			table[lin] = []
			for(col=0; col < COLUMNS; col++) {
				var tile = this.add.sprite(col*TILE_SIZE,lin*TILE_SIZE,"tile");
				tile.inputEnabled = true;
				tile.events.onInputDown.add(this.onTileClick, this);
				tile.tablePosition = { lin: lin, col: col };
				tile.isSelected = false;
				table[lin][col] = tile
			}
		}
	},
	onTileClick: function(tileSprite, pointer) {
		if(!canMove) {
			return;
		}
		canMove=false;

		if(this.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
			var player = playersGroup.children[0].castPlayer;
		} else if(this.input.keyboard.isDown(Phaser.Keyboard.UP)) {
			var player = playersGroup.children[1].castPlayer;
		} else if (this.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
			var player = playersGroup.children[2].castPlayer;
		} else if (this.input.keyboard.isDown(Phaser.Keyboard.DOWN)) {
			var player = playersGroup.children[3].castPlayer;
		} else {
			canMove = true;
			return;
		}

		if(!player || !player.sprite.alive) {
			canMove = true;
			return;
		}

		if(player.nextTile
				&& (player.nextTile.tablePosition.col != tileSprite.tablePosition.col
				|| player.nextTile.tablePosition.lin != tileSprite.tablePosition.lin)) {
			this.unselectTile(player.nextTile);
		}

		this.selectTile(tileSprite, player.playerData.name);

		// save next move
		player.nextTile = tileSprite

		if(this.isAllPlayersDone()) {
			this.runTurn();
		} else {
			canMove = true;
		}
		// sendMessage({
		// 	type: 'move',
		// 	player: player.name,
		// 	to: {
		// 		col: tileSprite.tablePosition.col,
		// 		lin: tileSprite.tablePosition.lin
		// 	}
		// });
	},
	isAllPlayersDone: function() {
		var players = gameManager.getPlayers();
		for (var i = 0; i < players.length; i++) {
			var player = players[i];
			if(player.sprite.alive && !player.nextTile) {
				return false;
			}
		};
		return true;
	},
	runTurn: function() {
		// players moves -> treat draws -> consume items
		this.runPlayersMove(function() {
			console.log("ALL DONE!");
			this.treatDrawMoves(function() {
				this.consumeItems(function() {
					this.createItem();
					canMove = true;
				});
			});
		});
	},
	consumeItems: function(callback) {
		var players = gameManager.getPlayers();
		for (var i = 0; i < players.length; i++) {
			var player = players[i];
			var item = this.getItemAt(player.playerData.tablePosition.col, player.playerData.tablePosition.lin);
			if(item) {
				this.consumeBomb(player, function() {
					item.destroy();
					this.consumeItems(callback);
				});
				return;
			}
		};
		callback.call(this);
	},
	runPlayersMove: function(callback) {
		var players = gameManager.getPlayers();
		for (var i = 0; i < players.length; i++) {
			var player = players[i];
			if(player.nextTile) {
				this.movePlayer(player, function() {
					this.runPlayersMove(callback);
				});
				return;
			}
		}
		callback.call(this);
	},
	treatDrawMoves: function(callback) {
		var movePlayerCallback = function() {
			this.treatDrawMoves(callback);
		}
		var players = gameManager.getPlayers();
		for (var i = 0; i < players.length; i++) {
			var p1 = player[i];
			for(var j=i+1; j < players.length; j++) {
				var p2 = players[j];
				if(p1.playerData.tablePosition.col === p2.playerData.tablePosition.col
						&& p1.playerData.tablePosition.lin === p2.playerData.tablePosition.lin) {
					// sort who must get out.
					var playerSentOff = Math.random() < 0.5 ? p1 : p2;
					this.hitPlayer(playerSentOff, 10);
					// get safe tile for playerSentOff
					safeTile = this.getSafeTileForPlayer(playerSentOff);
					playerSentOff.nextTile = safeTile;
					this.movePlayer(playerSentOff, movePlayerCallback);
					return;
				}
			}
		}
		callback.call(this);
	},
	getSafeTileForPlayer: function(player) {
		// choosing an empty tile around player
		do{
			var col = Math.floor(Math.random()*3) + player.playerData.tablePosition.col - 1;
			var lin = Math.floor(Math.random()*3) + player.playerData.tablePosition.lin - 1;
		} while (
				   lin < 0
				|| col < 0
				|| lin >= LINES
				|| col >= COLUMNS
				|| this.getPlayerAt(col,lin)
			)
		return table[lin][col];
	},
	getPlayerAt: function(col, lin) {
		var players = gameManager.getPlayers();
		for(var i=0; i < players.length; i++) {
			var player = players[i];
			if(player.playerData.tablePosition.col === col
					&& player.playerData.tablePosition.lin === lin) {
				return player;
			}
		}
		return false;
	},
	getItemAt: function(col, lin) {
		var items = itemsGroup.children;
		for(var i=0; i < items.length; i++) {
			var item = items[i];
			if(item.tablePosition.col === col && item.tablePosition.lin === lin) {
				return item;
			}
		}
		return false;
	},
	createPlayer: function(player) {
		// choosing an empty tile in the field
		do{
			var col = Math.floor(Math.random()*8);
			var lin = Math.floor(Math.random()*4);
		} while (this.getPlayerAt(col,lin))

		playerSprite = this.add.sprite(
				col*TILE_SIZE + TILE_SIZE/2,
				lin*TILE_SIZE + TILE_SIZE/2,
				"player"
			);
		// creation of a custom property "pos"
		player.playerData.tablePosition = {
			col: col,
			lin: lin
		};
		playerSprite.anchor.set(0.5);
		// using http://phaser.io/docs/2.4.4/Phaser.Component.Health.html#health
		playerSprite.health = 100;
		// creation of a text which will represent the player health
		var textSprite = this.add.text(
				0,
				TILE_SIZE/3,
				""+playerSprite.health,
				{ font:"bold 16px Arial", align:"center"}
			);
		// setting text anchor in the horizontal and vertical center
		textSprite.anchor.set(0.5);
		// adding the text as a child of tile sprite
		playerSprite.addChild(textSprite);


		// at the beginning the tile is completely transparent
		playerSprite.alpha=0;
		// creation of a new tween for the tile sprite
		var fadeIn = this.add.tween(playerSprite);
		// the tween will make the sprite completely opaque in 250 milliseconds
		fadeIn.to({alpha:1},250);
		// tween callback
		fadeIn.onComplete.add(function(){
			canMove=true;
		}, this);
		// starting the tween
		fadeIn.start();

		playersGroup.add(playerSprite);
		player.sprite = playerSprite;

		// sendMessage({
		// 	type: 'create_player',
		// 	player_name: player.name
		// });
	},
	createItem: function() {
		// choosing an empty tile in the field
		do{
			var col = Math.floor(Math.random()*8);
			var lin = Math.floor(Math.random()*4);
		} while (this.getPlayerAt(col,lin) || this.getItemAt(col, item))

		var item = this.add.sprite(
				col*TILE_SIZE + TILE_SIZE/2,
				lin*TILE_SIZE + TILE_SIZE/2,
				"bomb_item"
			);
		item.name = "bomb_item";
		// creation of a custom property "pos"
		item.tablePosition = {
			col: col,
			lin: lin
		};
		item.anchor.set(0.5);
		
		// at the beginning the tile is completely transparent
		item.alpha=0;
		// creation of a new tween for the tile sprite
		var fadeIn = this.add.tween(item);
		// the tween will make the sprite completely opaque in 250 milliseconds
		fadeIn.to({alpha:1},250);
		// starting the tween
		fadeIn.start();

		itemsGroup.add(item);
		return item;
	},
	// FUNCTION TO MOVE A PLAYER
	movePlayer: function(player, callback){
		var to = player.nextTile.tablePosition;
		// then we create a tween
		var movement = this.add.tween(player.sprite);
		var distance2= 1.0*(
				  Math.pow(to.lin - player.tablePosition.lin, 2)
				+ Math.pow(to.col - player.tablePosition.col, 2)
			);
		movement.to(
			{
				x:TILE_SIZE*to.col + TILE_SIZE/2,
				y:TILE_SIZE*to.lin + TILE_SIZE/2
			}
			, 200*Math.sqrt(distance2,2)
		);
		movement.onComplete.add(function(){
			this.unselectTile(table[player.playerData.tablePosition.lin][player.playerData.tablePosition.col]);
			if(callback) {
				callback.call(this);
			}
		}, this);
		player.playerData.tablePosition=to;
		player.nextTile = false;
		// let the tween begin!
		movement.start();
	},
	consumeBomb: function(player, callback) {
		var counter = 0;
		var callbackHub = function() {
			counter -= 1;
			console.log(counter);
			if (counter == 0) {
				callback.call(this);
			};
		};
		var tablePosition = player.playerData.tablePosition;
		if(tablePosition.col > 0) {
			if(tablePosition.lin > 0) {
				counter += 1;
				this.explode(tablePosition.col -1, tablePosition.lin -1, callbackHub);
			}
			if(tablePosition.lin < LINES - 1) {
				counter += 1;
				this.explode(tablePosition.col -1, tablePosition.lin +1, callbackHub);
			}
		}
		if(tablePosition.col < COLUMNS - 1) {
			if(tablePosition.lin > 0) {
				counter += 1;
				this.explode(tablePosition.col +1, tablePosition.lin -1, callbackHub);
			}
			if(tablePosition.lin < LINES - 1) {
				counter += 1;
				this.explode(tablePosition.col +1, tablePosition.lin +1, callbackHub);
			}
		}
	},
	explode: function(col, lin, callback) {
		var player = this.getPlayerAt(col,lin);
		if(player) {
			this.hitPlayer(player, 10);
		}

		// explosion animation
		var explosionAnimation = explosions.getFirstExists(false);
		explosionAnimation.reset(
				col*TILE_SIZE + TILE_SIZE/2,
				lin*TILE_SIZE + TILE_SIZE/2);
		explosionAnimation.animations.getAnimation('kaboom')
				.onComplete.addOnce(callback, this);
		explosionAnimation.play('kaboom', 30, false, true);
	},
	hitPlayer: function(player, damage) {
		player.sprite.damage(damage);
		var textSprite = player.sprite.children[0];
		textSprite.setText(player.sprite.health);
	},
	selectTile: function(tileSprite, text) {
		if(tileSprite.isSelected) {
			var textSprite = tileSprite.children[0];
			textSprite.setText(text);
		} else {
			// creation of a text which will represent the value of the tileSprite
			var textSprite = this.add.text(
					TILE_SIZE/2,
					TILE_SIZE/2,
					text,
					{ font:"bold 16px Arial", align:"center" }
				);
			// setting text anchor in the horizontal and vertical center
			textSprite.anchor.set(0.5);
			// adding the text as a child of tile sprite
			tileSprite.addChild(textSprite);

			// creation of a new tween for the tile sprite
			// IDLE -> SELECTED
			var clickOn = this.add.tween(tileSprite);
			clickOn.to({alpha:0.5},50);
			//clickOn.onComplete.add(function(){}, this);
			clickOn.start();
			tileSprite.isSelected = true;
		}
	},
	unselectTile: function(tileSprite) {
		if(tileSprite.isSelected) {
			// creation of a new tween for the tile sprite
			// SELECTED -> IDLE
			var clickOff = this.add.tween(tileSprite);
			clickOff.to({alpha:1.0},150);
			clickOff.start();

			tileSprite.children[0].destroy();
			tileSprite.isSelected = false;
		}
	}
};